@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ $bird->name }}</div>

                    <div class="card-body">
                        <ul>
                            <li>Nome: {{ $bird->name }}</li>
                            <li>Sexo: {{ $bird->getFullSex() }}</li>
                            <li>
                                Pai:
                                @if($bird->parent)
                                    <a href="{{ route('birds.show', $bird->parent->id) }}">{{ $bird->parent->name }}</a>
                                @endif
                            </li>
                            <li>
                                Mãe:
                                @if($bird->mother)
                                    <a href="{{ route('birds.show', $bird->mother->id) }}">{{ $bird->mother->name }}</a>
                                @endif
                            </li>
                            <li>Quantidade de filhos: {{ $bird->children->count() }}</li>
                        </ul>
                        <div class="col-md-12 text-md-right">
                            <a href="{{ route('home') }}" class="btn btn-primary">Voltar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center mt-3">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Árvore Genealógica</div>

                    <div class="card-body">
                        <div id="family_tree"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="{{ asset('js/loader.js') }}"></script>
    <script type="text/javascript">
        @if($bird->getFamilyTree())
            google.charts.load('current', {packages:["orgchart"]});
            google.charts.setOnLoadCallback(drawChart);

            function drawChart()
            {
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Name');
                data.addColumn('string', 'Manager');

                const familyTree = '{!! $bird->getFamilyTree() !!}';
                data.addRows(JSON.parse(familyTree));

                // Create the chart.
                var chart = new google.visualization.OrgChart(document.getElementById('family_tree'));
                // Draw the chart, setting the allowHtml option to true for the tooltips.
                chart.draw(data, {allowHtml:true});
            }
        @else
            document.getElementById('family_tree').appendChild("Sem Árvore Genealógica para este pássaro.")
        @endif
    </script>

@endsection
