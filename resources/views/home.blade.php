@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Cadastre um pássaro</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('birds.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-3 col-form-label text-md-right">Nome</label>

                            <div class="col-md-9">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-3 col-form-label text-md-right">Sexo</label>

                            <div class="col-md-9">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="sex" id="sex_m" value="M" @if(old('sex') == 'M') checked @endif>
                                    <label class="form-check-label" for="sex_m">
                                        Macho
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="sex" id="sex_f" value="F" @if(old('sex') == 'F') checked @endif>
                                    <label class="form-check-label" for="sex_f">
                                        Fêmea
                                    </label>
                                </div>

                                @error('sex')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="parent_id" class="col-md-3 col-form-label text-md-right">Pai</label>

                            <div class="col-md-9">
                                <select name="parent_id" id="parent_id" class="form-control @error('parent_id') is-invalid @enderror">
                                    <option value="">Indefinido</option>
                                    @foreach($birds->where('sex', 'M') as $parent)
                                        <option value="{{ $parent->id }}">{{ $parent->name }}</option>
                                    @endforeach
                                </select>

                                @error('parent_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="mother_id" class="col-md-3 col-form-label text-md-right">Mãe</label>

                            <div class="col-md-9">
                                <select name="mother_id" id="mother_id" class="form-control @error('mother_id') is-invalid @enderror">
                                    <option value="">Indefinido</option>
                                    @foreach($birds->where('sex', 'F') as $mother)
                                        <option value="{{ $mother->id }}">{{ $mother->name }}</option>
                                    @endforeach
                                </select>

                                @error('mother_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-9 offset-md-3 text-md-right">
                                <button type="submit" class="btn btn-primary">
                                    Salvar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center mt-3">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Pássaros Cadastrados</div>

                <div class="card-body">
                    @if($birds)
                        <ul>
                            @foreach($birds as $bird)
                                <li>
                                    <a href="{{ route('birds.show', [$bird->id]) }}">{{ $bird->name }}</a>
                                </li>
                            @endforeach
                        </ul>
                    @else
                        Não há pássaros cadastrados
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
