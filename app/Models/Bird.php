<?php

namespace Birds\Models;

use Birds\Enums\Sex;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Model;

class Bird extends Model
{
    protected $fillable = ['name', 'sex'];

    public function parent()
    {
        return $this->belongsTo(Bird::class, 'parent_id');
    }

    public function mother()
    {
        return $this->belongsTo(Bird::class, 'mother_id');
    }

    public function children()
    {
        if($this->sex == Sex::FEM)
            return $this->hasMany(Bird::class, 'mother_id');

        return $this->hasMany(Bird::class, 'parent_id');
    }

    public function getFullSex()
    {
        if($this->sex == Sex::FEM)
            return "Fêmea";

        return "Macho";
    }

    public function getFamilyTree($parentId=null)
    {
        $children = $this->children;

        $arr[] = [
            [
                'v' => "$this->id",
                'f' => '<a href=\"'.route("birds.show", $this->id).'\">'.$this->name.'</a>'
            ],
            is_null($parentId) ? '' : "$parentId"
        ];

        foreach ($children as $kid) {
            if($kid->children()->count() > 0) {
                $arr = array_merge($arr, $kid->getFamilyTree($this->id));
            } else {
                $arr[] = [
                    [
                        'v' => "$kid->id",
                        'f' => '<a href=\"'.route("birds.show", $kid->id).'\">'.$kid->name.'</a>'
                    ],
                    "$this->id"
                ];
            }
        }

        if(!is_null($parentId))
            return $arr;

        return json_encode($arr);
    }
}
