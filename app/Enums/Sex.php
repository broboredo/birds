<?php

namespace Birds\Enums;

class Sex
{
    const MASC = "M";
    const FEM = "F";
}
