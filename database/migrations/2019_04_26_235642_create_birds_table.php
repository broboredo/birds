<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBirdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('birds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->enum('sex', ['M', 'F']);
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->unsignedBigInteger('mother_id')->nullable();
            $table->timestamps();

            $table->foreign('parent_id')->references('id')->on('birds');
            $table->foreign('mother_id')->references('id')->on('birds');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('birds');
    }
}
